import { Component, OnInit } from '@angular/core';
import { faEnvelope } from "@fortawesome/free-regular-svg-icons";
import { faFacebookF, faTwitter, faInstagram, faPinterest } from "@fortawesome/free-brands-svg-icons";

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.sass']
})
export class NewsletterComponent implements OnInit {

  faEnvelope = faEnvelope;
  faFacebookF = faFacebookF;
  faTwitter = faTwitter;
  faInstagram = faInstagram;
  faPinterest = faPinterest;

  constructor() { }

  ngOnInit(): void {
  }

}
