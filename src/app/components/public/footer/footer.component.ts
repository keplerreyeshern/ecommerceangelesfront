import { Component, OnInit } from '@angular/core';
import { faMapMarkerAlt, faPhone } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope, faCreditCard } from "@fortawesome/free-regular-svg-icons";
import { faCcMastercard, faCcVisa, faCcAmex, faCcDiscover, faCcPaypal } from "@fortawesome/free-brands-svg-icons";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass']
})
export class FooterComponent implements OnInit {

  faEnvelope = faEnvelope;
  faMapMarkerAlt = faMapMarkerAlt;
  faPhone = faPhone;
  faCreditCard = faCreditCard;
  faCcMastercard = faCcMastercard;
  faCcVisa = faCcVisa;
  faCcAmex = faCcAmex;
  faCcDiscover = faCcDiscover;
  faCcPaypal = faCcPaypal;
  today:any;
  year:any;

  constructor() {
    this.today = new Date();
    this.year = this.today.getFullYear();
  }

  ngOnInit(): void {
  }

}
