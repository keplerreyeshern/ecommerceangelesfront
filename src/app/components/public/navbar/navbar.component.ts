import { Component, OnInit } from '@angular/core';
import { faPhone, faMapMarkerAlt, faDollarSign, faBars, faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope, faUser, faHeart } from "@fortawesome/free-regular-svg-icons";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {

  faPhone = faPhone;
  faMapMarkerAlt = faMapMarkerAlt;
  faDollarSign = faDollarSign;
  faEnvelope = faEnvelope;
  faUser = faUser;
  faHeart = faHeart;
  faShoppingCart = faShoppingCart;
  faBars = faBars;
  open:boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
