import { Component, OnInit } from '@angular/core';
import { faArrowAltCircleRight } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.sass']
})
export class MainComponent implements OnInit {

  faArrowAltCircleRight = faArrowAltCircleRight;

  constructor() { }

  ngOnInit(): void {
  }

}
