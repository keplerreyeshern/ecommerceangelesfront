import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { PublicComponentsModule } from "../../../components/public/public-components.module";


@NgModule({
  declarations: [
    MainComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    FontAwesomeModule,
    PublicComponentsModule
  ]
})
export class MainModule { }
