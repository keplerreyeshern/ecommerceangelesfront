import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { PublicComponent } from './public.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { SharedComponentsModule } from "../../components/shared/shared-components.module";
import { PublicComponentsModule } from "../../components/public/public-components.module";


@NgModule({
  declarations: [
    PublicComponent
  ],
  imports: [
    CommonModule,
    PublicRoutingModule,
    NgxSpinnerModule,
    SharedComponentsModule,
    PublicComponentsModule
  ]
})
export class PublicModule { }
